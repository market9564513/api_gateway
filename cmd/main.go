package main

import (
	"api_gateway/api"
	_ "api_gateway/api/docs"
	"api_gateway/api/handler"
	"api_gateway/config"
	"api_gateway/grpc/client"
	"api_gateway/pkg/logger"
	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	services, err := client.NewGrpcClient(cfg, log)
	if err != nil {
		log.Error("error is while initializing grpc clients", logger.Error(err))
		return
	}

	h := handler.New(services, log)

	r := api.New(h)

	log.Info("Server is running ", logger.Any("port:", cfg.HttpPort))
	if err := r.Run(cfg.HttpPort); err != nil {
		log.Error("error is while running port", logger.Error(err))
	}
}
