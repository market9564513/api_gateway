package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"log"
	"os"
)

type Config struct {
	ServiceName string
	LoggerLevel string
	Environment string

	HttpPort string

	PayGrpcServiceHost string
	PayGrpcServicePort string

	RepositoryGrpcServiceHost string
	RepositoryGrpcServicePort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		log.Println("cannot load .env file", err.Error())
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "pay_service"))
	cfg.LoggerLevel = cast.ToString(getOrReturnDefault("LOGGER_LEVEL", "debug"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.HttpPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	cfg.PayGrpcServiceHost = cast.ToString(getOrReturnDefault("PAY_GRPC_SERVICE_HOST", "localhost"))
	cfg.PayGrpcServicePort = cast.ToString(getOrReturnDefault("PAY_GRPC_SERVICE_PORT", ":8080"))

	cfg.RepositoryGrpcServiceHost = cast.ToString(getOrReturnDefault("REPOSITORY_GRPC_SERVICE_HOST", "localhost"))
	cfg.RepositoryGrpcServicePort = cast.ToString(getOrReturnDefault("REPOSITORY_GRPC_SERVICE_PORT", ":8080"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
