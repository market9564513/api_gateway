package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateCourier godoc
// @Router       /v1/courier [POST]
// @Summary      Create a new courier
// @Description  Create a new courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier  body models.CreateCourier false "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCourier(c *gin.Context) {
	request := pbp.CreateCourier{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.CourierService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  courier", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetCourier    godoc
// @Router       /v1/courier/{id} [GET]
// @Summary      Get courier  by id
// @Description  get courier  by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourier(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.CourierService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting courier by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetCourierList godoc
// @Router       /v1/couriers  [GET]
// @Summary      Get courier  list
// @Description  get courier list
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.CourierResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourierList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search := c.Query("search")

	resp, err := h.services.CourierService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get client list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateCourier godoc
// @Router       /v1/courier/{id} [PUT]
// @Summary      Update courier
// @Description  update courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Param        client  body models.UpdateCourier false "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourier(c *gin.Context) {
	request := pbp.Courier{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.CourierService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting courier by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteCourier godoc
// @Router       /v1/courier/{id} [DELETE]
// @Summary      Delete courier
// @Description  delete courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCourier(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.CourierService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete courier", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}
