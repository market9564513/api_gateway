package handler

import (
	pbr "api_gateway/genproto/repository_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateCategory godoc
// @Router       /v1/category [POST]
// @Summary      Create a new category
// @Description  Create a new category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        category body models.CreateCategory false "category"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCategory(c *gin.Context) {
	request := pbr.CreateCategory{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	fmt.Println("req", &request)
	category, err := h.services.CategoryService().Create(context.Background(), &request)
	if err != nil {
		fmt.Println("err", err.Error())
		h.log.Error("error is while creating category", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, category)
}

// GetCategory    godoc
// @Router       /v1/category/{id} [GET]
// @Summary      Get category by id
// @Description  get category by id
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category_id"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategory(c *gin.Context) {
	id := c.Param("id")

	category, err := h.services.CategoryService().Get(context.Background(), &pbr.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting category by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, category)
}

// GetCategoryList godoc
// @Router       /v1/categories [GET]
// @Summary      Get category list
// @Description  get category list
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.CategoryResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategoryList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	categories, err := h.services.CategoryService().GetList(context.Background(), &pbr.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get category list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, categories)
}

// UpdateCategory godoc
// @Router       /v1/category/{id} [PUT]
// @Summary      Update category
// @Description  update category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category"
// @Param        category body models.UpdateCategory false "category"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCategory(c *gin.Context) {
	category := pbr.Category{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&category); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	category.Id = uid

	updatedCategory, err := h.services.CategoryService().Update(context.Background(), &category)
	if err != nil {
		handleResponse(c, h.log, "error is while getting category by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, updatedCategory)
}

// DeleteCategory godoc
// @Router       /v1/category/{id} [DELETE]
// @Summary      Delete category
// @Description  delete category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCategory(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.CategoryService().Delete(context.Background(), &pbr.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete category", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "category deleted!")
}
