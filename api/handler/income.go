package handler

import (
	pbr "api_gateway/genproto/repository_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateIncome godoc
// @Router       /v1/income [POST]
// @Summary      Create a new income
// @Description  Create a new income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        income  body models.CreateIncome false "income"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateIncome(c *gin.Context) {
	request := pbr.CreateIncome{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.IncomeService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  income", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetIncome     godoc
// @Security ApiKeyAuth
// @Router       /v1/income/{id} [GET]
// @Summary      Get income  by id
// @Description  get income  by id
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        id path string true "income_id"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncome(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.GetIncome)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to get income", 400, err)
		return
	}

	id := c.Param("id")

	resp, err := h.services.IncomeService().Get(context.Background(), &pbr.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting income by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetIncomeList godoc
// @Security ApiKeyAuth
// @Router       /v1/incomes  [GET]
// @Summary      Get income  list
// @Description  get income list
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.IncomesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeList(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.GetIncomeList)
	if err != nil {
		fmt.Println("er", err.Error())
		handleResponse(c, h.log, "error while get auth to get list income", 400, err.Error())
		return
	}

	var (
		page, limit int
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeService().GetList(context.Background(), &pbr.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get income list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateIncome godoc
// @Security ApiKeyAuth
// @Router       /v1/income/{id} [PUT]
// @Summary      Update income
// @Description  update income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        id path string true "income_id"
// @Param        income  body models.UpdateIncome false "income"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateIncome(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.UpdateIncome)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to update income", 400, err)
		return
	}

	request := pbr.Income{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.IncomeService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting income by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteIncome  godoc
// @Router       /v1/income/{id} [DELETE]
// @Summary      Delete income
// @Description  delete income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        id path string true "income_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteIncome(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.IncomeService().Delete(context.Background(), &pbr.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete income", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "income deleted!")
}

// EndIncome godoc
// @Router       /v1/end-income/{id} [PUT]
// @Summary      Update end-income
// @Description  update end-income
// @Tags         income
// @Accept       json
// @Produce      json
// @Param        id path string true "income_id"
// @Success      201  {object}  models.Income
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) EndIncome(c *gin.Context) {
	id := c.Param("id")
	endIncome, err := h.services.IncomeService().EndIncome(context.Background(), &pbr.PrimaryKey{Id: id})
	if err != nil {
		handleResponse(c, h.log, "while ending income", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, endIncome)
}
