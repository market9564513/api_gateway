package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateStaff godoc
// @Router       /v1/staff [POST]
// @Summary      Create a new staff
// @Description  Create a new staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        staff  body models.CreateStaff false "staff"
// @Success      201  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateStaff(c *gin.Context) {
	request := pbp.CreateStaff{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.StaffService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  staff", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSale     godoc
// @Router       /v1/staff/{id} [GET]
// @Summary      Get staff  by id
// @Description  get staff  by id
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        id path string true "staff_id"
// @Success      201  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStaff(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.StaffService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting staff by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSaleList godoc
// @Router       /v1/staffs  [GET]
// @Summary      Get staff  list
// @Description  get staff list
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.StaffsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStaffList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.StaffService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get staff list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateSale godoc
// @Router       /v1/staff/{id} [PUT]
// @Summary      Update staff
// @Description  update staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        id path string true "staff_id"
// @Param        staff  body models.UpdateStaff false "staff"
// @Success      201  {object}  models.Staff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateStaff(c *gin.Context) {
	request := pbp.Staff{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.StaffService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteStaff  godoc
// @Router       /v1/staff/{id} [DELETE]
// @Summary      Delete staff
// @Description  delete staff
// @Tags         staff
// @Accept       json
// @Produce      json
// @Param        id path string true "staff_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteStaff(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.StaffService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete staff", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "staff deleted!")
}
