package handler

import (
	"api_gateway/api/models"
	"api_gateway/config"
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/jwt"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"net/http"
	"reflect"
	"time"
)

// Login godoc
// @Router       /v1/login [POST]
// @Summary      Login
// @Description  Login
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        login body models.LoginRequest false "login"
// @Success      201  {object}  models.LoginResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) Login(c *gin.Context) {
	var (
		loginRequest  = pbp.LoginRequest{}
		loginResponse = models.LoginResponse{}
	)

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AuthService().Login(ctx, &loginRequest)
	if err != nil {
		handleResponse(c, h.log, "error while customer login", http.StatusInternalServerError, err.Error())
		return
	}

	claims := map[string]interface{}{
		"user_id":   resp.GetId(),
		"user_role": resp.GetUserRole(),
	}

	accessToken, err := jwt.GenerateJWT(claims, config.AccessExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, h.log, "error while generating jwt", http.StatusInternalServerError, logger.Error(err))
		return
	}

	refreshToken, err := jwt.GenerateJWT(claims, config.RefreshExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, h.log, "error while generating jwt", http.StatusInternalServerError, logger.Error(err))
		return
	}

	loginResponse.AccessToken = accessToken
	loginResponse.RefreshToken = refreshToken

	handleResponse(c, h.log, "", http.StatusOK, loginResponse)
}

func (h Handler) GetAuthInfoFromToken(c *gin.Context, requiredPermission func(c *gin.Context)) (models.AuthInfo, error) {
	accessToken := c.GetHeader("Authorization")
	if accessToken == "" {
		return models.AuthInfo{}, config.ErrUnauthorized
	}

	claims, err := jwt.ExtractClaims(accessToken, string(config.SignKey))
	if err != nil {
		return models.AuthInfo{}, err
	}

	userID := cast.ToString(claims["user_id"])
	userRole := cast.ToString(claims["user_role"])

	//var (
	//	permission make(map()func(c *gin.Context))
	//)

	permission := make(map[int]func(c *gin.Context))

	hasPermission := false

	switch userRole {
	case "admin":
		hasPermission = true
	case "shop_assistant":
		permission[0] = h.ChangeStatus
		permission[1] = h.GetShift
		permission[2] = h.GetShiftList
	case "repo_admin":
		permission[0] = h.GetIncomeList
		permission[1] = h.GetIncome
		permission[2] = h.UpdateIncome
		permission[3] = h.GetIncomeProductList
		permission[4] = h.GetIncomeProduct
		permission[5] = h.UpdateIncomeProduct
	}

	for _, perm := range permission {
		if reflect.ValueOf(perm).Pointer() == reflect.ValueOf(requiredPermission).Pointer() {
			hasPermission = true
		}
	}

	if !hasPermission {
		return models.AuthInfo{}, config.ErrForbidden
	}

	return models.AuthInfo{
		UserID: userID,
	}, nil
}
