package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateSalePoint godoc
// @Router       /v1/sale_point [POST]
// @Summary      Create a new sale_point
// @Description  Create a new sale_point
// @Tags         sale_point
// @Accept       json
// @Produce      json
// @Param        sale_point  body models.CreateSalePoint false "sale_point"
// @Success      201  {object}  models.SalePoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSalePoint(c *gin.Context) {
	request := pbp.CreateSalesPoint{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.SalePointService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  sale point", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSalePoint     godoc
// @Router       /v1/sale_point/{id} [GET]
// @Summary      Get sale_point  by id
// @Description  get sale_point  by id
// @Tags         sale_point
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_point_id"
// @Success      201  {object}  models.SalePoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSalePoint(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.SalePointService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale_point by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSalePointList godoc
// @Router       /v1/sale_points  [GET]
// @Summary      Get sale_point  list
// @Description  get sale_point list
// @Tags         sale_point
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.SalePointsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSalePointList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePointService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get sale point list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateSalePoint godoc
// @Router       /v1/sale_point/{id} [PUT]
// @Summary      Update sale_point
// @Description  update sale_point
// @Tags         sale_point
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_id"
// @Param        sale_point  body models.UpdateSalePoint false "sale_point"
// @Success      201  {object}  models.SalePoint
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSalePoint(c *gin.Context) {
	request := pbp.SalesPoint{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.SalePointService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale point by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteSalePoint  godoc
// @Router       /v1/sale_point/{id} [DELETE]
// @Summary      Delete sale_point
// @Description  delete sale_point
// @Tags         sale_point
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_point_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSalePoint(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.SalePointService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete sale_point", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "sale_point deleted!")
}
