package handler

import (
	pbr "api_gateway/genproto/repository_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateIncomeProduct godoc
// @Router       /v1/income_product [POST]
// @Summary      Create a new income_product
// @Description  Create a new income_product
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        income_product  body models.CreateIncomeProduct false "income_product"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateIncomeProduct(c *gin.Context) {
	request := pbr.CreateIncomeProducts{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.IncomeProductService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  income product", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetIncomeProduct     godoc
// @Security ApiKeyAuth
// @Router       /v1/income_product/{id} [GET]
// @Summary      Get income_product  by id
// @Description  get income_product  by id
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        id path string true "income_product_id"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeProduct(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.GetIncomeProduct)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to get income product", 400, err)
		return
	}

	id := c.Param("id")

	resp, err := h.services.IncomeProductService().Get(context.Background(), &pbr.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting income product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetIncomeProductList godoc
// @Security ApiKeyAuth
// @Router       /v1/income_products  [GET]
// @Summary      Get income_product  list
// @Description  get income_product list
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.IncomeProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetIncomeProductList(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.GetIncomeProductList)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to get income", 400, err)
		return
	}

	var (
		page, limit int
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeProductService().GetList(context.Background(), &pbr.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get income product list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateIncomeProduct godoc
// @Security ApiKeyAuth
// @Router       /v1/income_product/{id} [PUT]
// @Summary      Update income_product
// @Description  update income_product
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        id path string true "income_product_id"
// @Param        income_product  body models.UpdateIncomeProduct false "income_product"
// @Success      201  {object}  models.IncomeProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateIncomeProduct(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.UpdateIncomeProduct)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to update income", 400, err)
		return
	}

	request := pbr.IncomeProduct{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.IncomeProductService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting income product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteIncomeProduct  godoc
// @Router       /v1/income_product/{id} [DELETE]
// @Summary      Delete income_product
// @Description  delete income_product
// @Tags         income_product
// @Accept       json
// @Produce      json
// @Param        id path string true "income_product_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteIncomeProduct(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.IncomeProductService().Delete(context.Background(), &pbr.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete income product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "income product deleted!")
}
