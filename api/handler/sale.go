package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"strings"
)

// CreateSale godoc
// @Router       /v1/sale [POST]
// @Summary      Create a new sale
// @Description  Create a new sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        sale  body models.CreateSale false "sale"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSale(c *gin.Context) {
	request := pbp.CreateSale{}
	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.services.SaleService().Create(context.Background(), &request)
	if err != nil {
		if strings.Contains(err.Error(), "you cannot sale because opened shift does not exist") {
			handleResponse(c, h.log, "you cannot sale because opened shift does not exist", http.StatusBadRequest, err.Error())
			return
		}
		handleResponse(c, h.log, "error while creating sale", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSale     godoc
// @Router       /v1/sale/{id} [GET]
// @Summary      Get sale  by id
// @Description  get sale  by id
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_id"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSale(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.SaleService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSaleList godoc
// @Router       /v1/sales  [GET]
// @Summary      Get sale  list
// @Description  get sale list
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.SalesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get income list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateSale godoc
// @Router       /v1/sale/{id} [PUT]
// @Summary      Update sale
// @Description  update sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_id"
// @Param        sale  body models.UpdateSale false "sale"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSale(c *gin.Context) {
	request := pbp.Sale{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.SaleService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteSale  godoc
// @Router       /v1/sale/{id} [DELETE]
// @Summary      Delete sale
// @Description  delete sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSale(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.SaleService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete sale", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "sale deleted!")
}

// EndSale godoc
// @Router       /v1/end-sale/{id} [PUT]
// @Summary      Update sale
// @Description  update sale
// @Tags         sale
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_id"
// @Success      201  {object}  models.Sale
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) EndSale(c *gin.Context) {
	id := c.Param("id")
	endSaleData, err := h.services.SaleService().EndSale(context.Background(), &pbp.PrimaryKey{Id: id})
	if err != nil {
		handleResponse(c, h.log, "while ending sale", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, endSaleData)
}
