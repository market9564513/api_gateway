package handler

import (
	pbr "api_gateway/genproto/repository_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateStorage    godoc
// @Router       /v1/storage [POST]
// @Summary      Create a new storage
// @Description  Create a new storage
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        storage  body models.CreateStorage false "storage"
// @Success      201  {object}  models.Storage
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateStorage(c *gin.Context) {
	request := pbr.CreateStorage{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.StorageService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  storage", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetStorage     godoc
// @Router       /v1/storage/{id} [GET]
// @Summary      Get storage  by id
// @Description  get storage  by id
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        id path string true "storage_id"
// @Success      201  {object}  models.Storage
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStorage(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.StorageService().Get(context.Background(), &pbr.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting storage by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetStorageList godoc
// @Router       /v1/storage  [GET]
// @Summary      Get storage  list
// @Description  get storage list
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.StorageResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetStorageList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.StorageService().GetList(context.Background(), &pbr.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get storage list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateStorage godoc
// @Router       /v1/storage/{id} [PUT]
// @Summary      Update storage
// @Description  update storage
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        id path string true "storage"
// @Param        sale_point  body models.UpdateStorage false "storage"
// @Success      201  {object}  models.Storage
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateStorage(c *gin.Context) {
	request := pbr.Storage{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.StorageService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting storage by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteStorage  godoc
// @Router       /v1/storage/{id} [DELETE]
// @Summary      Delete storage
// @Description  delete storage
// @Tags         storage
// @Accept       json
// @Produce      json
// @Param        id path string true "storage_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteStorage(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.StorageService().Delete(context.Background(), &pbr.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete storage", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "storage deleted!")
}
