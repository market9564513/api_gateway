package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateShift    godoc
// @Router       /v1/shift [POST]
// @Summary      Create a new shift
// @Description  Create a new shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        shift  body models.CreateShift false "shift"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateShift(c *gin.Context) {
	request := pbp.CreateShift{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.ShiftService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  shift", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetShift     godoc
// @Security ApiKeyAuth
// @Router       /v1/shift/{id} [GET]
// @Summary      Get shift  by id
// @Description  get shift  by id
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift_id"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetShift(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.GetShift)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to get shift", 400, err)
		return
	}
	id := c.Param("id")

	resp, err := h.services.ShiftService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting shift by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetShiftList godoc
// @Security ApiKeyAuth
// @Router       /v1/shifts  [GET]
// @Summary      Get shift  list
// @Description  get shift list
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.ShiftsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetShiftList(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.GetShiftList)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to get list shift", 400, err)
		return
	}
	var (
		page, limit int
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get shift list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateShift godoc
// @Router       /v1/shift/{id} [PUT]
// @Summary      Update shift
// @Description  update shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift_id"
// @Param        shift  body models.UpdateShift false "shift"
// @Success      201  {object}  models.Shift
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateShift(c *gin.Context) {
	request := pbp.Shift{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.ShiftService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting shift by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteShift  godoc
// @Router       /v1/shift/{id} [DELETE]
// @Summary      Delete shift
// @Description  delete shift
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteShift(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.ShiftService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete shift", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "shift deleted!")
}

// ChangeStatus godoc
// @Security ApiKeyAuth
// @Router       /v1/change_status/{id} [PATCH]
// @Summary      Update change_status
// @Description  update change_status
// @Tags         shift
// @Accept       json
// @Produce      json
// @Param        id path string true "shift_id"
// @Param        shift  body models.UpdateStatus false "shift"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) ChangeStatus(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c, h.ChangeStatus)
	if err != nil {
		handleResponse(c, h.log, "error while get auth to update status shift", 400, err)
		return
	}

	id := c.Param("id")
	request := pbp.Shift{}
	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = id
	response, err := h.services.ShiftService().UpdateStatus(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "while updating status", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, response)
}
