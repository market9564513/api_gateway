package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateBranch godoc
// @Router       /v1/branch [POST]
// @Summary      Create a new branch
// @Description  Create a new branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        branch  body models.CreateBranch false "branch"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranch(c *gin.Context) {
	request := pbp.Branch{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}

	resp, err := h.services.BranchService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  branch", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetBranch    godoc
// @Router       /v1/branch/{id} [GET]
// @Summary      Get branch  by id
// @Description  get branch  by id
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_id"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranch(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.BranchService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting branch by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetBranchList godoc
// @Router       /v1/branches  [GET]
// @Summary      Get branch  list
// @Description  get branch list
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.BranchesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	search := c.Query("search")
	resp, err := h.services.BranchService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get branch list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateBranch godoc
// @Router       /v1/branch/{id} [PUT]
// @Summary      Update branch
// @Description  update branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_id"
// @Param        branch  body models.UpdateBranch false "branch"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranch(c *gin.Context) {
	request := pbp.Branch{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.BranchService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting branch by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteBranch godoc
// @Router       /v1/branch/{id} [DELETE]
// @Summary      Delete branch
// @Description  delete branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranch(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.BranchService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete branch", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}
