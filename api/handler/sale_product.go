package handler

import (
	pbp "api_gateway/genproto/pay_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"strings"
)

// CreateSaleProduct godoc
// @Router       /v1/sale_product [POST]
// @Summary      Create a new sale_product
// @Description  Create a new sale_product
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        sale_product  body models.CreateSaleProduct false "sale_product"
// @Success      201  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateSaleProduct(c *gin.Context) {
	request := pbp.CreateSaleProduct{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.SaleProductService().Create(context.Background(), &request)
	if err != nil {
		if strings.Contains(err.Error(), "in storage not enough product") {
			handleResponse(c, h.log, "in storage not enough product", 400, err.Error())
			return
		}
		handleResponse(c, h.log, "while creating sale product", http.StatusInternalServerError, err.Error())
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSaleProduct     godoc
// @Router       /v1/sale_product/{id} [GET]
// @Summary      Get sale_product  by id
// @Description  get sale_product  by id
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_product_id"
// @Success      201  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleProduct(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.SaleProductService().Get(context.Background(), &pbp.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetSaleProductList godoc
// @Router       /v1/sale_products  [GET]
// @Summary      Get sale_product  list
// @Description  get sale_product list
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      201  {object}  models.SaleProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetSaleProductList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().GetList(context.Background(), &pbp.GetRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: c.Query("search"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get sale product list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateSaleProduct godoc
// @Router       /v1/sale_product/{id} [PUT]
// @Summary      Update sale_product
// @Description  update sale_product
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_product_id"
// @Param        sale_product  body models.UpdateSaleProduct false "sale_product"
// @Success      201  {object}  models.SaleProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateSaleProduct(c *gin.Context) {
	request := pbp.SaleProduct{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.SaleProductService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting sale product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteSaleProduct  godoc
// @Router       /v1/sale_product/{id} [DELETE]
// @Summary      Delete sale_product
// @Description  delete sale_product
// @Tags         sale_product
// @Accept       json
// @Produce      json
// @Param        id path string true "sale_product_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteSaleProduct(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.SaleProductService().Delete(context.Background(), &pbp.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete sale", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "sale deleted!")
}
