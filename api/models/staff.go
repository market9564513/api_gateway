package models

type Staff struct {
	ID           string `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Phone        string `json:"phone"`
	Login        string `json:"login"`
	Password     string `json:"password"`
	SalesPointID string `json:"sales_point_id"`
	UserRole     string `json:"user_role"`
	CreatedAt    string `json:"created_at"`
	UpdatedAt    string `json:"updated_at"`
}

type CreateStaff struct {
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Phone        string `json:"phone"`
	Login        string `json:"login"`
	Password     string `json:"password"`
	SalesPointID string `json:"sales_point_id"`
	UserRole     string `json:"user_role"`
}

type UpdateStaff struct {
	ID           string `json:"-"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Phone        string `json:"phone"`
	Login        string `json:"login"`
	Password     string `json:"password"`
	SalesPointID string `json:"sales_point_id"`
}

type StaffsResponse struct {
	Staffs []Staff `json:"staffs"`
	Count  int32   `json:"count"`
}
