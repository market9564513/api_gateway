package models

type ChangePassword struct {
	Login       string `json:"login"`
	NewPassword string `json:"new_password"`
	OldPassword string `json:"old_password"`
}

type PrimaryKey struct {
	ID string `json:"id"`
}

type GetRequest struct {
	Page   int32  `json:"page"`
	Limit  int32  `json:"limit"`
	Search string `json:"search"`
}

type Msg struct {
	Msg string `json:"msg"`
}

type MultiUpdatePriceRequest struct {
	Prices map[string]int32 `json:"prices"`
}

type MultipleRequest struct {
	NewProducts map[string]Storage `json:"new_products"`
}

type MultipleUpdateRequest struct {
	Products map[string]Storage `json:"products"`
}
