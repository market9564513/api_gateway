package models

type Income struct {
	ID        string `json:"id"`
	CourierID string `json:"courier_id"`
	Status    string `json:"status"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type CreateIncome struct {
	CourierID string `json:"courier_id"`
	Status    string `json:"status"`
}

type UpdateIncome struct {
	ID        string `json:"-"`
	CourierID string `json:"courier_id"`
	Status    string `json:"status"`
}

type IncomesResponse struct {
	Incomes []Income `json:"incomes"`
	Count   int32    `json:"count"`
}
