package models

type SalePoint struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	BranchID  string `json:"branch_id"`
	IncomeID  string `json:"income_id"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type CreateSalePoint struct {
	Name     string `json:"name"`
	BranchID string `json:"branch_id"`
	IncomeID string `json:"income_id"`
}

type UpdateSalePoint struct {
	ID       string `json:"-"`
	Name     string `json:"name"`
	BranchID string `json:"branch_id"`
	IncomeID string `json:"income_id"`
}

type SalePointsResponse struct {
	SalePoints []SalePoint `json:"sale_points"`
	Count      int32       `json:"count"`
}
