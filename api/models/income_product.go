package models

type IncomeProduct struct {
	ID          string `json:"id"`
	IncomeID    string `json:"income_id"`
	CategoryID  string `json:"category_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Quantity    int32  `json:"quantity"`
	IncomePrice int32  `json:"income_price"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type CreateIncomeProduct struct {
	IncomeID    string `json:"income_id"`
	CategoryID  string `json:"category_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Quantity    int32  `json:"quantity"`
	IncomePrice int32  `json:"income_price"`
}

type UpdateIncomeProduct struct {
	ID          string `json:"-"`
	IncomeID    string `json:"income_id"`
	CategoryID  string `json:"category_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Quantity    int32  `json:"quantity"`
	IncomePrice int32  `json:"income_price"`
}

type IncomeProductsResponse struct {
	IncomeProducts []IncomeProduct `json:"income_products"`
	Count          int32           `json:"count"`
}
