package models

type Shift struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	StaffID     string `json:"staff_id"`
	SalePointID string `json:"sale_point_id"`
	StartHour   string `json:"start_hour"`
	EndHour     string `json:"end_hour"`
	Status      string `json:"status"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type CreateShift struct {
	Name        string `json:"name"`
	StaffID     string `json:"staff_id"`
	SalePointID string `json:"sale_point_id"`
	StartHour   string `json:"start_hour"`
	EndHour     string `json:"end_hour"`
	Status      string `json:"status"`
}

type UpdateShift struct {
	ID          string `json:"-"`
	Name        string `json:"name"`
	StaffID     string `json:"staff_id"`
	SalePointID string `json:"sale_point_id"`
	StartHour   string `json:"start_hour"`
	EndHour     string `json:"end_hour"`
	Status      string `json:"status"`
}

type ShiftsResponse struct {
	SalePoints []SalePoint `json:"sale_points"`
	Count      int32       `json:"count"`
}

type UpdateStatus struct {
	Status string `json:"status"`
}
