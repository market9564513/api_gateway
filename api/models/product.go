package models

type Product struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	CategoryID string `json:"category_id"`
	Barcode    string `json:"barcode"`
	Price      int32  `json:"price"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

type CreateProduct struct {
	Name       string `json:"name"`
	CategoryID string `json:"category_id"`
	Price      int32  `json:"price"`
}

type UpdateProduct struct {
	ID         string `json:"-"`
	Name       string `json:"name"`
	CategoryID string `json:"category_id"`
	Price      int32  `json:"price"`
}

type ProductsResponse struct {
	Products []Product `json:"products"`
	Count    int32     `json:"count"`
}
