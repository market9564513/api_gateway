package models

type Storage struct {
	ID          string `json:"id"`
	SalePointID string `json:"sale_point_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	Barcode     string `json:"barcode"`
	IncomePrice int32  `json:"income_price"`
	Quantity    int32  `json:"quantity"`
	TotalPrice  int32  `json:"total_price"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type CreateStorage struct {
	SalePointID string `json:"sale_point_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	IncomePrice int32  `json:"income_price"`
	Quantity    int32  `json:"quantity"`
	TotalPrice  int32  `json:"total_price"`
}

type UpdateStorage struct {
	ID          string `json:"id"`
	SalePointID string `json:"sale_point_id"`
	ProductID   string `json:"product_id"`
	ProductName string `json:"product_name"`
	IncomePrice int32  `json:"income_price"`
	Quantity    int32  `json:"quantity"`
	TotalPrice  int32  `json:"total_price"`
}

type StorageResponse struct {
	Storage []Storage `json:"storage"`
	Count   int32     `json:"count"`
}
