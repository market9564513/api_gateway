package models

type SaleProduct struct {
	ID                   string `json:"id"`
	SaleID               string `json:"sale_id"`
	ProductID            string `json:"product_id"`
	Barcode              string `json:"barcode"`
	Quantity             int32  `json:"quantity"`
	ExistProductQuantity int32  `json:"exist_product_quantity"`
	Price                int32  `json:"price"`
	TotalPrice           int32  `json:"total_price"`
	CreatedAt            string `json:"created_at"`
	UpdatedAt            string `json:"updated_at"`
}

type CreateSaleProduct struct {
	SaleID    string `json:"sale_id"`
	ProductID string `json:"product_id"`
	Quantity  int32  `json:"quantity"`
}

type UpdateSaleProduct struct {
	ID         string `json:"-"`
	SaleID     string `json:"sale_id"`
	ProductID  string `json:"product_id"`
	Quantity   int32  `json:"quantity"`
	Price      int32  `json:"-"`
	TotalPrice int32  `json:"-"`
}

type SaleProductsResponse struct {
	SaleProducts []SaleProduct `json:"sale_products"`
	Count        int32         `json:"count"`
}
