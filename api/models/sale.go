package models

type Sale struct {
	ID          string `json:"id"`
	Barcode     string `json:"barcode"`
	ShiftID     string `json:"shift_id"`
	SalePointID string `json:"sale_point_id"`
	StaffID     string `json:"staff_id"`
	Status      string `json:"status"`
	Payment     string `json:"payment"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type CreateSale struct {
	ShiftID     string `json:"shift_id"`
	SalePointID string `json:"sale_point_id"`
	StaffID     string `json:"staff_id"`
	Status      string `json:"status"`
	Payment     string `json:"payment"`
}

type UpdateSale struct {
	ID          string `json:"-"`
	ShiftID     string `json:"shift_id"`
	SalePointID string `json:"sale_point_id"`
	StaffID     string `json:"staff_id"`
	Status      string `json:"status"`
	Payment     string `json:"payment"`
}

type SalesResponse struct {
	Sales []Sale `json:"sales"`
	Count int32  `json:"count"`
}
