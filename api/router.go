package api

import (
	_ "api_gateway/api/docs"
	"api_gateway/api/handler"
	"fmt"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"log"
	"time"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	// middleware
	r.Use(traceRequest)

	v1Route := r.Group("/v1")

	// auth endpoints
	v1Route.POST("/login", h.Login)

	// branch
	v1Route.POST("/branch", h.CreateBranch)
	v1Route.GET("/branch/:id", h.GetBranch)
	v1Route.GET("/branches", h.GetBranchList)
	v1Route.PUT("/branch/:id", h.UpdateBranch)
	v1Route.DELETE("/branch/:id", h.DeleteBranch)

	// category
	v1Route.POST("/category", h.CreateCategory)
	v1Route.GET("/category/:id", h.GetCategory)
	v1Route.GET("/categories", h.GetCategoryList)
	v1Route.PUT("/category/:id", h.UpdateCategory)
	v1Route.DELETE("/category/:id", h.DeleteCategory)

	// courier
	v1Route.POST("/courier", h.CreateCourier)
	v1Route.GET("/courier/:id", h.GetCourier)
	v1Route.GET("/couriers", h.GetCourierList)
	v1Route.PUT("/courier/:id", h.UpdateCourier)
	v1Route.DELETE("/courier/:id", h.DeleteCourier)

	// income
	v1Route.POST("/income", h.CreateIncome)
	v1Route.GET("/income/:id", h.GetIncome)
	v1Route.GET("/incomes", h.GetIncomeList)
	v1Route.PUT("/income/:id", h.UpdateIncome)
	v1Route.DELETE("/income/:id", h.DeleteIncome)
	v1Route.PUT("/end-income/:id", h.EndIncome)

	// income_product
	v1Route.POST("/income_product", h.CreateIncomeProduct)
	v1Route.GET("/income_product/:id", h.GetIncomeProduct)
	v1Route.GET("/income_products", h.GetIncomeProductList)
	v1Route.PUT("/income_product/:id", h.UpdateIncomeProduct)
	v1Route.DELETE("/income_product/:id", h.DeleteIncomeProduct)

	// product
	v1Route.POST("/product", h.CreateProduct)
	v1Route.GET("/product/:id", h.GetProduct)
	v1Route.GET("/products", h.GetProductList)
	v1Route.PUT("/product/:id", h.UpdateProduct)
	v1Route.DELETE("/product/:id", h.DeleteProduct)

	// sale
	v1Route.POST("/sale", h.CreateSale)
	v1Route.GET("/sale/:id", h.GetSale)
	v1Route.GET("/sales", h.GetSaleList)
	v1Route.PUT("/sale/:id", h.UpdateSale)
	v1Route.DELETE("/sale/:id", h.DeleteSale)
	v1Route.PUT("/end-sale/:id", h.EndSale)

	// sale_point
	v1Route.POST("/sale_point", h.CreateSalePoint)
	v1Route.GET("/sale_point/:id", h.GetSalePoint)
	v1Route.GET("/sale_points", h.GetSalePointList)
	v1Route.PUT("/sale_point/:id", h.UpdateSalePoint)
	v1Route.DELETE("/sale_point/:id", h.DeleteSalePoint)

	// sale_product
	v1Route.POST("/sale_product", h.CreateSaleProduct)
	v1Route.GET("/sale_product/:id", h.GetSaleProduct)
	v1Route.GET("/sale_products", h.GetSaleProductList)
	v1Route.PUT("/sale_product/:id", h.UpdateSaleProduct)
	v1Route.DELETE("/sale_product/:id", h.DeleteSaleProduct)

	// shift
	v1Route.POST("/shift", h.CreateShift)
	v1Route.GET("/shift/:id", h.GetShift)
	v1Route.GET("/shifts", h.GetShiftList)
	v1Route.PUT("/shift/:id", h.UpdateShift)
	v1Route.DELETE("/shift/:id", h.DeleteShift)
	v1Route.PATCH("/change_status/:id", h.ChangeStatus)

	// staff
	v1Route.POST("/staff", h.CreateStaff)
	v1Route.GET("/staff/:id", h.GetStaff)
	v1Route.GET("/staffs", h.GetStaffList)
	v1Route.PUT("/staff/:id", h.UpdateStaff)
	v1Route.DELETE("/staff/:id", h.DeleteStaff)

	// storage
	v1Route.POST("/storage", h.CreateStorage)
	v1Route.GET("/storage/:id", h.GetStorage)
	v1Route.GET("/storage", h.GetStorageList)
	v1Route.PUT("/storage/:id", h.UpdateStorage)
	v1Route.DELETE("/storage/:id", h.DeleteStorage)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}

func traceRequest(c *gin.Context) {
	beforeRequest(c)

	c.Next()

	afterRequest(c)
}

func beforeRequest(c *gin.Context) {
	startTime := time.Now()

	c.Set("start_time", startTime)

	log.Println("start time:", startTime.Format("2006-01-02 15:04:05.000"), "path:", c.Request.URL.Path)
}

func afterRequest(c *gin.Context) {
	startTime, exists := c.Get("start_time")
	if !exists {
		startTime = time.Now()
	}

	duration := time.Since(startTime.(time.Time)).Seconds()

	log.Println("end time:", time.Now().Format("2006-01-02 15:04:05.000"), "duration:", duration, "method:", c.Request.Method)
	fmt.Println()
}
